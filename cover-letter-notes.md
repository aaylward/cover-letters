cover letter notes:
chip-seq data processing
de-novo gene discovery from RNA-seq
SNP calling

My standard procedure is to implement a pipeline as a software package which
can be used generally by data analysts with minimal extra effort.

Going from scratch to a functioning pipeline is a matter of months

I am just beginning to develop a professional network and can offer services
at a discount.

I am told BioRad can budget 100k for a project of several months (6).

That budget and timeline is sufficient for me to develop an efficient, robust,
and easy-to-use software system.

One of the standout qualities of my work is user interfaces. Bioinformatics
analyses should be easy to apply and reproduce without extensive technical
knowledge.

In my lab, my analyses of ChIP-seq and ATAC-seq data have been converted to
millions of dollars in grant awards

(https://fox5sandiego.com/2019/08/13/ucsd-team-receives-9-million-grant-for-diabetes-research/)

The pipeline would be added to my portfolio & hence would recieve active
support even after the end of the contract.

Project might involve setup of computational infrastructure (server, database,
web portal, etc).

I am writing to offer my service as a bioinformatics consultant

Setting an exact timeframe will require a more detailed consultation about the work to be done.

Chip-Seq/ATAC-Seq
SNP analysis
RNA-seq / de-novo transcript discovery
